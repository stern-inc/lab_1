import random
from datetime import datetime
import kr_lib as kr


def matrixmult(m1, m2):
    n_m = float(datetime.strftime(datetime.now(), "%M "))
    n_s = float(datetime.strftime(datetime.now(), "%S.%f"))
    s = 0  # сумма
    t = []  # временная матрица
    m3 = []  # конечная матрица
    if len(m2) != len(m1[0]):
        print("Матрицы не могут быть перемножены")
    else:
        r1 = len(m1)  # количество строк в первой матрице
        c1 = len(m1[0])  # Количество столбцов в 1
        r2 = c1  # и строк во 2ой матрице
        c2 = len(m2[0])  # количество столбцов во 2ой матрице
        for z in range(0, r1):
            for j in range(0, c2):
                for i in range(0, c1):
                    s = s + m1[z][i] * m2[i][j]
                t.append(s)
                s = 0
            m3.append(t)
            t = []
    return kr.secundomer(n_m, n_s)


minim = 10
step = 10
maxim = 500
results = []
while minim <= maxim:
    r1 = minim
    c1 = minim
    r2 = minim
    c2 = minim
    g = matrixmult(kr.create_float_matrix(r1, c1), kr.create_float_matrix(r2, c2)), minim
    results.append(g)
    print(f'{minim} finaly')
    minim += step
print(results)
kr.mass_to_file(results, 'Время', 'Кол-во')
