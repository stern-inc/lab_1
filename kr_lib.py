import random
from datetime import datetime
import matplotlib.pyplot as plt


def plot_data(x_arg, y_arg, legend='f(x)', title='Plot f(x)', x_label='x', y_label='f(x)'):
    plt.plot(x_arg, y_arg, 'ko-')
    plt.legend([legend])
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()


def secundomer(o_m, o_s):
    nm, ns = float(datetime.strftime(datetime.now(), "%M ")), float(datetime.strftime(datetime.now(), "%S.%f"))
    o_t = o_m * 60 + o_s
    n_t = nm * 60 + ns
    return n_t - o_t


def mass_printer(mass):
    for element in mass:
        print('{:}'.format(element), end=' ')


def create_int_matrix(r, c):
    t = []
    m = []
    for i in range(0, r):
        for j in range(0, c):
            t.append(random.randint(1, 10))
        m.append(t)
        t = []
    return m


def create_float_matrix(r, c):
    t = []
    m = []
    for i in range(0, r):
        for j in range(0, c):
            t.append(random.uniform(1, 10))
        m.append(t)
        t = []
    return m


def mass_to_file(mass, f_t, s_t):
    with open('data.txt', 'w') as f:
        print(f'{f_t}\t{s_t}', file=f)
        i = 0
        while i < len(mass):
            print(f'{mass[i[0]]}\t{mass[i[1]]}', file=f)
            i += 1
