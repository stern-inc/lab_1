import random
from datetime import datetime
import matplotlib.pyplot as plt
import kr_lib as kr
import time


def mass_to_file(f_m, s_m):
    with open('data.txt', 'w') as f:
        print('Кол-во\tВремя', file=f)
        i = 0
        while i < len(f_m):
            print(f'{f_m[i]}\t{s_m[i]}', file=f)
            i += 1


def is_point_in_area(point, mid, radius):
    return (float(mid[0]) - float(point[0])) ** 2 + (float(mid[1]) - float(point[1])) ** 2 <= radius ** 2


def point_counter(points, mid, radius):
    points_counter = 0
    for point in points:
        if is_point_in_area(point, mid, radius):
            points_counter += 1
    return points_counter


n_min = 10e4
n_max = 10e5
n_step = 10e4

n_data = []
time_data = []

for n in range(int(n_min), int(n_max), int(n_step)):
    averages = []
    points = [(random.uniform(-10, 10), random.uniform(-10, 10)) for i in range(n)]
    center = (random.uniform(-10, 10), random.uniform(-10, 10))
    rad = n / 4
    for _ in range(3):
        start = time.time()
        temp = point_counter(points, center, rad)
        averages.append(time.time() - start)
    average = sum(averages) / 3
    print(average)
    n_data.append(n)
    time_data.append(average)
print(n_data)
print(time_data)
mass_to_file(n_data, time_data)
