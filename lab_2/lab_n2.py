import math


x, a, fcn_num = 0, 0, 0
try:
    x, a, fcn_num = float(input('Enter "x": ')), float(input('Enter "a": ')), \
                    int(input('Enter function number [1/2/3]: '))
except ValueError:
    print('Enter number, not letter')
    exit(1)
if fcn_num == 1:
    try:
        G = (4 * (-18 * a ** 2 + 3 * a * x + 10 * x ** 2) / (15 * a ** 2 + 29 * a * x + 12 * x ** 2))
        print('G: {:.3f}'.format(G))
    except ZeroDivisionError:
        print('ERROR ZERO DIVISION\nG: NO ')
elif fcn_num == 2:
    F = 1 / (math.cos(5 * a**2 + 14 * a * x - 3 * x**2))
    print('F:', F)
elif fcn_num == 3:
    try:
        Y = math.acos(9 * a ** 2 + 42 * a * x + 40 * x ** 2 + 1)
        print('Y: {:.3f}'.format(Y))
    except ValueError:
        print('ValueError\nY: NO')
else:
    print('Error: operation number != [1/2/3]')
