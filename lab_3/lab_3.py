import math
import matplotlib.pyplot as plt


def plot_data(x, y, lgnd='f(x)', title='Plot f(x)', xlabel='x', ylabel='f(x)'):
    plt.plot(x, y, 'ko-')
    plt.legend([lgnd])
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.show()


while True:
    X_mass, G_mass, F_mass, Y_mass = [], [], [], []
    a, x_l, x_u, fcn_num, x_method = 0, 0, 0, 0, 0

    try:
        a = float(input('Введите "a": '))
        x_l, x_u = float(input('Нижняя граница "x": ')), float(input('Верхняя граница "x": '))
        fcn_num = int(input('Введите номер функции [1-G/2-F/3-Y]: '))
        x_method = int(input('[1 -   Вы введёте шаги | 2 - Вы введёте шаг]: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    x_steps, x_step, x = 0, 0, x_l

    try:
        if x_method == 1:
            x_steps = int(input('Введите ЦЕЛОЕ число шагов: '))
            x_step = (x_u - x_l) / x_steps
        elif x_method == 2:
            x_step = float(input('Введите значение шага: '))
    except ValueError:
        print('Enter number, not letter')
        exit(1)

    if fcn_num == 1:
        while x <= x_u:
            try:
                G = (4 * (-18 * a ** 2 + 3 * a * x + 10 * x ** 2) / (15 * a ** 2 + 29 * a * x + 12 * x ** 2))
                print('---\nG: {:.3f} || X: {:.3f} |'.format(G, x))
                G_mass.append(G)
                X_mass.append(x)
            except ZeroDivisionError:
                G_mass.append(None)
                X_mass.append(x)
                print('---\n ERROR ZERO DIVISION\nG: NO || X: {:.3f} |'.format(x))
            x += x_step
        plot_data(X_mass, G_mass, 'G(x)', 'Plot G(x)', 'x', 'G(x)')
    elif fcn_num == 2:
        while x <= x_u:
            F = 1 / (math.cos(5 * a ** 2 + 14 * a * x - 3 * x ** 2))
            print('---\nF: {:.3f} || X: {:.3f} |'.format(F, x))
            x += x_step
            X_mass.append(x)
            F_mass.append(F)
        plot_data(X_mass, F_mass, 'F(x)', 'Plot F(x)', 'x', 'F(x)')

    elif fcn_num == 3:
        while x <= x_u:
            try:
                Y = math.acos(9 * a ** 2 + 42 * a * x + 40 * x ** 2 + 1)
                print('---\nY: {:.3f} || X: {:.3f} |'.format(Y, x))
                X_mass.append(x)
                Y_mass.append(Y)
            except ValueError:
                print('---\nValueError\nY: NO || X: {:.3f} |'.format(x))
                X_mass.append(x)
                Y_mass.append(None)
            x += x_step
        plot_data(X_mass, Y_mass, 'Y(x)', 'Plot Y(x)', 'x', 'Y(x)')
    else:
        print('Error: номер операции не равен 1/2/3')

    while True:
        try:
            a = int(input("\nХотите повторить вычисления снова? 1 - Да | 0 - Нет "))
            break
        except ValueError:
            print('Ввод не распознан. Повторите еще раз.')
        continue

    if a == 0:
        print('Have a nice day! :)')
        break
